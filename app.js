const http = require('http');
const express = require('express');
const fs = require('fs');
const path = require('path');
const app = express();
const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "text/html");
  res.end("MUI-E API webhandler");
});

/* Load Configuration */
const config = JSON.parse(fs.readFileSync(path.join(
  __dirname,
  'config.json'
)));

/* Global variables */
var MyTimer = 0;
var timerInterval;
var MarathonActive = 0;

// HTTP Server listener
server.listen(config.webport, () => {
  console.log(`Web server running on port ${config.webport}`);
});
// API Listener
app.listen(config.port, () => {
  console.log(`MUI-E server running on port ${config.port}`);
});
// Blank API listener on port...
app.get("/", (req, res) => {
  res.send("MUI-E API");
});

/* Timer Function */
function MyTimerCount() {
  if(MyTimer > 0) {
    MyTimer -= 1;
  }
  console.log(`Timer: ${MyTimer}`);
  if(MyTimer == 0) {
    clearInterval(timerInterval);
    MarathonActive = 0;
  }
}

/* API ENDPOINTS */
/* Bits to Seconds */
app.get("/b2s/:amount/:persec", (req, res) => {
  var b2s = Number(req.params.amount) * Number(req.params.persec);
  res.send(`${b2s}`);
});
/* Marathon Timer */
/* Timer Get */
app.route('/timer')
  .get(function(req, res) {
    res.send(`${MyTimer}`);
  })
app.route('/timer/mui')
  .get(function(req, res) {
    var hours = Math.floor((MyTimer % (60 * 60 * 24)) / (60 * 60));
    var minutes = Math.floor((MyTimer % (60 * 60)) / 60);
    var seconds = Math.floor((MyTimer % (60)));
    var resString = "";
    if(hours >= 1) {
      if(hours < 10) {
        resString += "0";
      }
      resString += `${hours}:`;
    } else {
      resString += `00:`;
    }
    if(minutes >= 1) {
      if(minutes < 10) {
        resString += "0";
      }
      resString += `${minutes}:`;
    } else {
      resString += `00:`;
    }
    if(seconds >= 1) {
      if(seconds < 10) {
        resString += "0";
      }
      resString += `${seconds}`;
    } else {
      resString += `00`;
    }
    res.send(resString + ` remain on the timer.`);
  })

/* Timer Add */
app.get("/timer/add/:username/:amount", (req, res) => {
  console.log("API: Recieved request for /timeradd");
  MyTimer = Number(MyTimer) + Number(req.params.amount);
  console.log(`Added ${req.params.amount} to Marathon Timer`);
  res.send(`Added ${req.params.amount} to Marathon Timer`);
});
/* Timer Set */
app.get('/timer/set/:hours/:minutes/:seconds', (req, res) => {
  console.log("API: Recieved request for /timerset");
  if(MarathonActive == 1) {
    res.send(`Cannot set active timer.`);
  } else {
    MyTimer = 0;
    // Seconds
    MyTimer = Number(MyTimer) + Number(req.param.seconds);
    // Minutes
    MyTimer = Number(MyTimer) + (Number(req.param.minutes)*60);
    // Hours
    MyTimer = Number(MyTimer) + (Number(req.param.hours)*60);
    console.log(`Timer set to ${MyTimer} seconds.`);
    res.send(`Timer set to ${MyTimer} seconds.`);
  }
});
/* Timer Start */
app.get('/timer/start', (req, res) => {
  console.log('API: Recieved request for /timerstart');
  if(MarathonActive == 1) {
    console.log(`Marathon Timer currently running.  Skipping request...`);
    res.send(`Timer currently running.`);
  } else {
    console.log(`Marathon Timer active.`);
    MarathonActive = 1;
    timerInterval = setInterval(MyTimerCount, 1000);
    res.send(`Timer started!`);
  }
});
/* Timer Pause */
app.get('/timer/pause', (req, res) => {
  clearInterval(MyTimerCount);
})